"""
This script generates an RSA key pair(private and public keys), and saves them to separate PEM files.

Steps:
1. Generate a 2048-bit RSA private key with a public exponent of 65537.
2. Extract the corresponding public key from the private key.
3. Save the private key to a file named `private_RSA_key.pem` in PEM format without encryption.
4. Save the public key to a file named `public_RSA_key.pem` in PEM format.
5. Print the public key content.

Modules:
    - rsa: Generates RSA keys.
    - serialization: Used to serialize keys to PEM format.
    - default_backend: Provides cryptographic backend.

Outputs:
    - "private_RSA_key.pem": File containing the private key in PEM format.
    - "public_RSA_key.pem": File containing the public key in PEM format.
    - The public key is also printed to the console.
"""

from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends import default_backend

# Generate the private key
private_key = rsa.generate_private_key(
    public_exponent=65537,
    key_size=2048,
    backend=default_backend()
)

# Extract the public key from the private key
public_key = private_key.public_key()

# Save the private key to a PEM file
private_pem = private_key.private_bytes(
    encoding=serialization.Encoding.PEM,
    format=serialization.PrivateFormat.TraditionalOpenSSL,
    encryption_algorithm=serialization.NoEncryption()
)
with open("private_RSA_key.pem", "wb") as f:
    f.write(private_pem)

# Save the public key to a PEM file
public_pem = public_key.public_bytes(
    encoding=serialization.Encoding.PEM,
    format=serialization.PublicFormat.SubjectPublicKeyInfo
)
with open("public_RSA_key.pem", "wb") as f:
    f.write(public_pem)

print(public_pem)
