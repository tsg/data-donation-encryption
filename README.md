# Add encryption to Data donation

## Introduction
In case data should be **_stored_** encrypted, and you are not using the Surf Research cloud solution, which already provides such a solotion, the following alternative workaround can be applied. 

## Data donation project
https://github.com/d3i-infra/data-donation-task


## Adjustments needed
- Extras needed for encryption: <br>
   copy public_RSA_key.pem file to public folder <br>
   make following code changes to worker_engine.ts:<br>
   ```
   # addd the followinf import to worker_engine.ts
   import { encryptMessage } from './encrypt_message'
   ```
    ```
  # replace handleRunCycle function (end of file worker_engine.ts) with:
  handleRunCycle (command: any): void {
      if (isCommand(command)) {
          if (command.__type__ === 'CommandSystemDonate') {
              // apply AES encryption
              command.json_string = encryptMessage(command.json_string)
          }
          this.commandHandler.onCommand(command).then(
            (response) => this.nextRunCycle(response),
            () => {}
          )
       }
  }
    ```

   add file: encrypt_message.ts to folder src/framework/processing/<br>
   add node packages:
    - node install node-forge
    - node install crypto-js