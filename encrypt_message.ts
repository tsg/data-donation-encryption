
// Packages needed for symmetric (CryptoJS) and asymmetric encryption (node-forge)
const CryptoJS = require("crypto-js"); // NB: npm install crypto-js
const forge = require('node-forge'); // NB: npm install node-forge

// const publicKeyPem = 
// `-----BEGIN PUBLIC KEY-----
// .................................................................
// -----END PUBLIC KEY-----`

let publicKeyPem: string
// Lees de public key uit het PEM-bestand
loadPublicKey().then(publicKey => {
    publicKeyPem = publicKey
    console.log('Public key: ', publicKeyPem.substring(0,Math.min(50,publicKeyPem.length)));
})


export function encryptMessage(data: string): string {
    // 1. Create random symmetric AES-key
    const aesKey = CryptoJS.lib.WordArray.random(32); // 32 bytes = 256 bits
    const iv = CryptoJS.lib.WordArray.random(16); // 16 bytes = 128 bits
    // Convert AES-key to hex-string
    const aesKeyHex = aesKey.toString(CryptoJS.enc.Hex);

    // Convert hex-string to byte array (forge bytes)
    const aesKeyBytes = forge.util.hexToBytes(aesKeyHex);

    // Encrypt the data with AES-key
    const encryptedData = CryptoJS.AES.encrypt(data, aesKey, { iv: iv }).toString();

    // Convert RSA publicKey to correct format
    const publicKey = forge.pki.publicKeyFromPem(publicKeyPem);

    // Encrypt the AES-key using the RSA public key from receiver (publicKeyPem)
    const encryptedAesKey = publicKey.encrypt(aesKeyBytes, 'RSA-OAEP', {
        md: forge.md.sha256.create()
    });

    // Encode encrypted AES-key in Base64
    const encryptedAesKeyBase64 = forge.util.encode64(encryptedAesKey);

    // Convert IV to hex-string for transmission
    const ivHex = iv.toString(CryptoJS.enc.Hex);

    // Compose JSON-object for transmission
    const jsonObject = {
        data: encryptedData,
        aesKey: encryptedAesKeyBase64,
        iv: ivHex
    };

    // Serialise to JSON string
    const jsonString = JSON.stringify(jsonObject);

    return jsonString;
}


async function loadPublicKey(): Promise<string> {
    // Wait for fetch call to get the response
    const response = await fetch('./public_RSA_key.pem');

    // Check correct succesfull response
    if (!response.ok) {
        throw new Error('Network response was not ok ' + response.statusText);
    }
    
    // Wait for response result
    const publicKeyPem = await response.text();

    // Return public key
    return publicKeyPem;
}
