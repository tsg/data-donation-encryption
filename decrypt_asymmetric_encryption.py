"""
This script shows how to process encrypted Data Donation Package (DDP) data. It first deencrypts the symmetric AES key
using the specified private RSA key. DDP encrypted data will then be deencrypted using this AES key. Further table processing
is highly specific to how the Data Donation information is packed together. Use it as an example.
"""
from cryptography.hazmat.primitives import serialization
# NB: we need to distinguish between the asymmetric and symmetric padding functions!
#     therefore, they are each imported as asympadding and sympadding instead of padding
from cryptography.hazmat.primitives.asymmetric import padding as asympadding
from cryptography.hazmat.primitives import padding as sympadding
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
import base64
import json
import pandas as pd
import sys


def process_table(tbl):
    """
    Converts a table (in dictionary format) to a CSV format and prints it.

    Parameters:
    tbl (dict or any): The table data to process, typically in dictionary format. If it is not a dictionary, it will be printed as-is.

    If the table is a dictionary, it attempts to convert it into a CSV using pandas.
    In case of any exception during the conversion, the error message and the original table are printed.
    """
    if isinstance(tbl, dict):
        try:
            csv_output = pd.DataFrame(tbl).to_csv()
            print(csv_output)
        except Exception as e:
            # cannot convert to table, just print it
            print(f"Error while converting table to CSV: {e}")
            print(tbl)
    else:
        print(tbl)


def process_tables(decrypted_data):
    """
    Processes JSON data containing one or more tables. Each table is processed individually.

    Parameters:
    decrypted_data (str): A JSON-formatted string containing one or more tables. This can be either a list of tables or a single table.

    The function attempts to load and parse the JSON string. If it's not a list, it wraps it into a list and processes each table using `process_table`.
    """
    try:
        tables = json.loads(decrypted_data)
    except json.JSONDecodeError as e:
        print(f"Failed to decode JSON: {e}")
        return

    if not isinstance(tables, list):
        tables = [tables]

    for tbl in tables:
        process_table(tbl)


def process_file(datadonation_content, key_filename):
    """
    Decrypts the AES-encrypted content from a DataDonation message using an RSA private key.

    Parameters:
    datadonation_content (dict): A dictionary containing the encrypted data, AES key, and initialization vector (IV).
    key_filename (str): Path to the RSA private key file in PEM format.

    The function performs the following steps:
    1. Reads and decodes the Base64-encoded AES key.
    2. Reads and converts the hex-encoded IV.
    3. Loads the RSA private key from the provided PEM file.
    4. Decrypts the AES key using the RSA private key with OAEP padding.
    5. Uses the decrypted AES key and IV to decrypt the actual data (assumed to be AES-encrypted).
    6. Removes PKCS7 padding from the decrypted data.
    7. Processes the decrypted tables by calling `process_tables`.
    """
    encrypted_data = datadonation_content['data']

    # Read and convert the Base64 coded AES-key to bytes
    encrypted_aes_key = datadonation_content['aesKey']
    encrypted_aes_key_bytes = base64.b64decode(encrypted_aes_key)

    # Read and convert IV from hex to bytes
    iv_hex = datadonation_content['iv']
    iv = bytes.fromhex(iv_hex)

    with open(key_filename, 'rb') as key_file:
        private_key_pem = key_file.read()

    # Convert private key into correct format
    private_key = serialization.load_pem_private_key(
        private_key_pem,
        password=None,
        backend=default_backend()
    )

    # Decrypt the AES-key with RSA private key
    aes_key_bytes = private_key.decrypt(
        encrypted_aes_key_bytes,
        asympadding.OAEP(
            mgf=asympadding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )

    # Decrypt data with AES-key
    cipher = Cipher(algorithms.AES(aes_key_bytes),
                    modes.CBC(iv), backend=default_backend())
    decryptor = cipher.decryptor()
    decrypted_data = decryptor.update(
        base64.b64decode(encrypted_data)) + decryptor.finalize()

    # Remove PKCS7 padding
    unpadder = sympadding.PKCS7(algorithms.AES.block_size).unpadder()
    decrypted_data = unpadder.update(decrypted_data) + unpadder.finalize()

    process_tables(decrypted_data)


def main(data_filename, RSA_key_filename):
    """
    Main entry point to process encrypted DataDonation files.

    Parameters:
    data_filename (str): The path to the JSON file containing encrypted DataDonation messages.
    RSA_key_filename (str): The path to the RSA private key file used to decrypt the AES keys.

    The function performs the following steps:
    1. Loads the JSON content from the data file.
    2. Detects if the content comes from different storage solutions (Eyra or Radcloud) and processes accordingly.
    3. Each DataDonation message is decrypted, and its contents are processed.
    """
    with open(data_filename, 'r') as file:
        file_content = json.load(file)

    # The file_content results in a list of received DataDonation messages
    # Each message is a dictionary with key: 'results'
    #   The 'results' dictionary has keys: 'key', '__type__', and 'json_string'
    #       'key'           [str] --> '<a number>-tracking' unique DataDonation session identifier
    #       '__type__'      [str] --> always equals 'CommandSystemDonate'
    #       'json_string'   [str] --> contains the AES encrypted DataDonation content
    #
    # Decryption of the 'json_string' DataDonation content, the dictionary contains:
    #   'data'      []          --> Donated content
    #   'aesKey'    [base64]    --> SRA encrypted AES key used to encrypt the donated content (data)
    #   'iv'        [hex]       --> iv initiation vector for Cipher Block Chaining

    if isinstance(file_content, dict) and 'aesKey' in file_content.keys():
        # from Eyra data storage
        process_file(file_content, RSA_key_filename)
    else:
        # from Radcloud data storage (NB: Radboud university specific data storage solution)
        for message in file_content:
            try:
                datadonation_content = json.loads(
                    message['results']['json_string'])
            except Exception as e:
                print(
                    "====================== FAILURE parsing message =========================")
                print(e)
                print(message)
                print(
                    "========================================================================")
                continue

            process_file(datadonation_content, RSA_key_filename)


if __name__ == "__main__":
    # python decrypt_asymmetric_encryption.py <path to DDP json file>  <path to private key pem file>

    # Check input arguments
    if len(sys.argv) != 3:
        print('Usage: python decrypt_asymmetric_encryption.py <filename with encrypted json data> <filename that contains private RSA key>')
    else:
        data_filename = sys.argv[1]  # filename is in second argument
        RSA_key_filename = sys.argv[2]   # private RSA key in third argument
        main(data_filename, RSA_key_filename)
